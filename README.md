# Learn Octave

>**Educational resources for GNU Octave / MATLAB**  
>Octave for scientific computing and technical applications. Octave is an open-source, high-level programming language designed to provide syntactic compatability with the MathWorks MATLAB language. Primarily used for statistical computation, Octave is optimized for working with numbers and matrices on a large scale. When installed with `gnuplot`, Octave also provides graphing functionality for visual analysis and reporting.

This repository is maintained by [Octave-Matlab](mailto:octave-matlab@protonmail.com)
