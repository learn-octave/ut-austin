%Reverting an array of numbers
%Enter [1 2 3 4 5] and kick out [5 4 3 2 1]

%Enter the array
NumOfElements=input('How many elements do you want? -> ');
for Index=1:NumOfElements
    %eneting elements
    MyArrayForReverting(Index)=input(['Enter element no. ' num2str(Index) ' -> ']);
end
%Done entering array

%Now reverting the array!
for RevertingIndex=NumOfElements:-1:1
    %Creating the reverted array
    RevertedArray(NumOfElements+1-RevertingIndex)=MyArrayForReverting(RevertingIndex);
end

%Another way for reverting an array
%Not really needed - but good to see it!
for AnotherRevIndex=1:NumOfElements
    AnotherRevertedArray(AnotherRevIndex)=MyArrayForReverting(NumOfElements+1-AnotherRevIndex);
end

