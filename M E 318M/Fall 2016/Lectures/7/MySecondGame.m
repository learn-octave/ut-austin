%This is my second game (September 15, 2016)
PlayGame=true;

while PlayGame
    disp('Playing game...')
    pause(2) %pause 2 seconds
    disp('Game Over!!!')
    %Game is over at this point
    %Start prompting if the user wants to play again
    
    UserAnswer=input('Do you want to play again? -> ','s');
    if strcmp(UserAnswer,'y')|strcmp(UserAnswer,'yes')|strcmp(UserAnswer,'Yes')|strcmp(UserAnswer,'YES')
        disp('Yeeeey, let''s play the game!')
        PlayGame=true; %Keep the game key true (keep playing)
        %We will do the game here
    elseif strcmp(UserAnswer,'n')|strcmp(UserAnswer,'no')|strcmp(UserAnswer,'NO')|strcmp(UserAnswer,'No')
        disp('Why do you leave me...')
        PlayGame=false;
        %End the game here
    else
        disp('I do not know what you''re saying')
        PlayGame=0; %Same as PlayGame=false
        %Handle the situation when you do not know the answer
    end
    
end
    