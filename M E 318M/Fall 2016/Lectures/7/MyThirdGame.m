%This is my third game (September 15, 2016)
%Game goes on until the user says 'no' to the prompt
%"do you want to keep playing?". THe improvement over the
%second game is that now, if the user enters something we
%do not understand, he/she will be prompted again until we
%obtain an answer we understand!!!

PlayGame=true;

while PlayGame
    disp('Playing game...')
    pause(2) %pause 2 seconds
    disp('Game Over!!!')
    %Game is over at this point
    %Start prompting if the user wants to play again
    
    KeepAnswering=true;
    while KeepAnswering %Loop for entering user input about
        %wanting to play the game!
        UserAnswer=input('Do you want to play again? -> ','s');
        if strcmp(UserAnswer,'y')|strcmp(UserAnswer,'yes')|strcmp(UserAnswer,'Yes')|strcmp(UserAnswer,'YES')
            disp('Yeeeey, let''s play the game!')
            KeepAnswering=false;
            PlayGame=true; %Keep the game key true (keep playing)
            %We will do the game here
        elseif strcmp(UserAnswer,'n')|strcmp(UserAnswer,'no')|strcmp(UserAnswer,'NO')|strcmp(UserAnswer,'No')
            disp('Why do you leave me...')
            KeepAnswering=false;
            PlayGame=false;
            %End the game here
        else
            disp('I do not know what you''re saying')
            KeepAnswering=true;
            PlayGame=0; %Same as PlayGame=false
            %Handle the situation when you do not know the answer
        end
    end
    
end
    