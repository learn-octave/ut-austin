%This program will ask the user how many elements
%of an array he/she wants to enter. Then, we will
%enter those elements. Then we will display the sum
%of all those elements, the range of all those elements
%max of all those elements and min of al those elements!

clear %clear all variables so they do not mess us
NoOfElements=input('How many elements do you want to enter -> ');
%Entering those elements
for Index=1:NoOfElements
    Array(Index)=input(['Enter element number ' num2str(Index) ' -> ']);
end %Done entering elements of my array

SumOfAllElements=sum(Array); %built in function summing up elements in an array!
RangeOfAllElements=range(Array); %built in function finding range of all elements!
MaxOfAllElements=max(Array); %function for max
MinOfAllElements=min(Array); %function for min

%You complete this to enable displaying of these results!
disp(['Sum of all elements is ' num2str(SumOfAllElements) '.'])


