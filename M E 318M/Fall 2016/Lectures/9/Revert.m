function [OutputArray,N]=Revert(InputArray);
%This function reverts InputArray e.g.
%[1 2 3 4] is transformed into [4 3 2 1].
%Reverted array is output via variable OutputArray.
%The function also outputs length of the array
%via variable N.

LengthOfTheArray=length(InputArray);
for Index=1:LengthOfTheArray
    OutputArray(Index)=InputArray(LengthOfTheArray+1-Index);
end
N=LengthOfTheArray;

