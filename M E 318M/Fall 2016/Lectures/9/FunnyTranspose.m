function [FunnyMatrix]=FunnyTranspose(Matrix);
%Does a funny transpose from the class (fill in the blanks yourself).

[NumOfRows,NumOfCols]=size(Matrix);

if NumOfRows==NumOfCols
    %It's a square matrix, do the funny transpose!
    for Ind1=1:NumOfRows %or NumOfCols - it's the same here!
        for Ind2=1:NumOfRows %or NumOfCols - it's the same here!
            M(Ind1,Ind2)=Matrix(NumOfRows+1-Ind2,NumOfRows+1-Ind1); %funny transposing
        end
    end
    FunnyMatrix=M;
        
else
    %It's NOT a square matrix, don't do the funny transpose!
    disp('Warning! Matrix not square, funny transpose not done. Funny transpose is assigned to be []')
    FunnyMatrix=[];
end
