function [Sum]=MyFirstSumFunction(Array);
%Takes in an array (Array) and outputs SUM
%of all its elements (Product).

N=length(Array);Sum=0;
for Index=1:N
    Sum=Sum+Array(Index);
end
