function [Product]=MyFirstProductFunction(Array);
%Takes in an array (Array) and outputs products
%of all its elements (Product).

N=length(Array);Product=1;
for Index=1:N
    Product=Product*Array(Index);
end
