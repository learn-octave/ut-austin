%Multiplying Numbers Up

LowerNumber=input('Input the lower limit for multiplication -> ');
UpperNumber=input('Input the upper limit for multiplication -> ');

Product = 1; %Initializing the product.

for Index=LowerNumber:UpperNumber
    Product=Product*Index; %Updating the product
end
%Done with multiplying. Now displaying!
DisplayString=['Product of integers between ' num2str(LowerNumber) ' and ' num2str(UpperNumber) ' is ' num2str(Product) '.'];
disp(DisplayString)