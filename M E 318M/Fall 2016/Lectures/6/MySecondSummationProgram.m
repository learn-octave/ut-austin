%Summing Up Numbers
%While loop will handle a user who enters numbers wrongly.
%Meaning, if a user enters LowerNumber which is bigger than the
%bigger number, we'll prompt the user to re-enter the numbers!

KeepPrompting=true; %at the start of the program, I should enter
%into the loop for entering numbers!!!
while KeepPrompting
    LowerNumber=input('Input the lower limit for summation -> ');
    UpperNumber=input('Input the upper limit for summation -> ');
    %check if the entries were proper (upper number is bigger than
    %the lower number).
    if LowerNumber>UpperNumber
        KeepPrompting=true;
        disp('Your upper number is smaller than the lower number. Please re-enter your numbers properly.')
    else
        KeepPrompting=false;
        disp('Thank you for entering numbers properly!')
    end
end


Sum=0;
for Index=LowerNumber:UpperNumber
    Sum=Sum+Index;
end

disp(['The sum is ' num2str(Sum) '.'])
