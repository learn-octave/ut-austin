%Summing Up Numbers
%First, let's sum up numbers from 1 to 50

LowerNumber=input('Input the lower limit for summation -> ');
UpperNumber=input('Input the upper limit for summation -> ');

Sum=0;
for Index=LowerNumber:UpperNumber
    Sum=Sum+Index;
end

disp(['The sum is ' num2str(Sum) '.'])
